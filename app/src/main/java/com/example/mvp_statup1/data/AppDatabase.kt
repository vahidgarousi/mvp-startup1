package com.example.mvp_statup1.data

import android.arch.persistence.room.*
import android.content.*

@Database(entities = arrayOf(News::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getLocalDataSource(): LocalDataSource

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        private const val DATABASE_NAME = "mvp-startup1.db"
        private val LOCK = Object()

        fun getInstance(context: Context): AppDatabase {
            if (instance == null) {
                synchronized(LOCK) {
                    instance =
                        Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                            .setJournalMode(JournalMode.TRUNCATE)
                            .build()
                }
            }
            return instance as AppDatabase
        }
    }
}