package com.example.mvp_statup1.data

import android.arch.persistence.room.*
import io.reactivex.*

@Dao
abstract class LocalDataSource : NewsDataSource {

    @Query("SELECT * FROM `tbl_news`")
    abstract override fun getNews(): Flowable<List<News>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun saveNewsList(newsList: List<News>)

    override fun getVideoNews(): Single<List<News>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getBanners(): Single<List<Banner>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun bookmark(news: News) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun search(keyword: String): Single<News> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}