
package com.example.mvp_statup1.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
@Entity(tableName = "tbl_news")
public class News implements Parcelable {

    @Expose
    private String content;
    @Expose
    private String date;
    @Expose
    @PrimaryKey()
    private Long id;
    @Expose
    private String image;
    @Expose
    private String title;
    @Expose
    private String video;

    private boolean isVideoNews;

    public boolean isVideoNews() {
        return isVideoNews;
    }

    public void setVideoNews(boolean videoNews) {
        isVideoNews = videoNews;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.content);
        dest.writeString(this.date);
        dest.writeValue(this.id);
        dest.writeString(this.image);
        dest.writeString(this.title);
        dest.writeString(this.video);
        dest.writeByte(this.isVideoNews ? (byte) 1 : (byte) 0);
    }

    public News() {
    }

    protected News(Parcel in) {
        this.content = in.readString();
        this.date = in.readString();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.image = in.readString();
        this.title = in.readString();
        this.video = in.readString();
        this.isVideoNews = in.readByte() != 0;
    }

    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        @Override
        public News createFromParcel(Parcel source) {
            return new News(source);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };

    @Override
    public String toString() {
        return "News{" +
                "content='" + content + '\'' +
                ", date='" + date + '\'' +
                ", id=" + id +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", video='" + video + '\'' +
                ", isVideoNews=" + isVideoNews +
                '}';
    }
}
