package com.example.mvp_statup1.data

import android.content.*
import io.reactivex.*
import io.reactivex.functions.*
import io.reactivex.schedulers.*

class NewsRepository(var context: Context) : NewsDataSource {
    private var cloudDataSource = CloudDataSource()
    private lateinit var localDataSource: LocalDataSource

    init {
        localDataSource = AppDatabase.getInstance(context).getLocalDataSource()
    }

    override fun getNews(): Flowable<List<News>> {
        cloudDataSource.getNews()
            .subscribeOn(Schedulers.newThread())
            .observeOn(Schedulers.newThread())
            .doOnNext(object : Consumer<List<News>> {
                override fun accept(newsList: List<News>?) {
                    localDataSource.saveNewsList(newsList!!)
                }
            })
            .subscribe()
        return localDataSource.getNews()
    }

    override fun getVideoNews(): Single<List<News>> {
        return cloudDataSource.getVideoNews()
    }

    override fun getBanners(): Single<List<Banner>> {
        return cloudDataSource.getBanners()
    }

    override fun bookmark(news: News) {
        localDataSource.bookmark(news)
    }

    override fun search(keyword: String): Single<News> {
        return localDataSource.search(keyword)
    }
}