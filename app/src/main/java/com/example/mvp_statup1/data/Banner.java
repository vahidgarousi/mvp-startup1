
package com.example.mvp_statup1.data;

import com.google.gson.annotations.Expose;

@SuppressWarnings("unused")
public class Banner {

    @Expose
    private String image;
    @Expose
    private String source;
    @Expose
    private String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
