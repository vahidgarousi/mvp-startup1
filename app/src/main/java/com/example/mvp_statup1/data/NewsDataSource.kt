package com.example.mvp_statup1.data

import io.reactivex.*

interface NewsDataSource {
    fun getNews(): Flowable<List<News>>
    fun getVideoNews(): Single<List<News>>
    fun getBanners(): Single<List<Banner>>
    fun bookmark(news: News)
    fun search(keyword: String): Single<News>
}