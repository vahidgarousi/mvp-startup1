package com.example.mvp_statup1.data

import io.reactivex.*
import retrofit2.http.*

interface EnglishApiService {

    @GET("p5gwn")
    fun getNews(): Flowable<List<News>>

    @GET("tdr8j")
    fun getViewNews(): Single<List<News>>

    @GET("el9en")
    fun getBanners(): Single<List<Banner>>
}