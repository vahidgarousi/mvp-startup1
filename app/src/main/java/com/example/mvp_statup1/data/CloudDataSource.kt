package com.example.mvp_statup1.data

import io.reactivex.*
import retrofit2.*
import retrofit2.adapter.rxjava2.*
import retrofit2.converter.gson.*

class CloudDataSource : NewsDataSource {
    private  var englishApiService: EnglishApiService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.myjson.com/bins/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        englishApiService = retrofit.create(EnglishApiService::class.java)
    }

    override fun getNews(): Flowable<List<News>> {
        return englishApiService.getNews()
    }

    override fun getVideoNews(): Single<List<News>> {
        return englishApiService.getViewNews()
    }

    override fun getBanners(): Single<List<Banner>> {
        return englishApiService.getBanners()
    }

    override fun bookmark(news: News) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun search(keyword: String): Single<News> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}