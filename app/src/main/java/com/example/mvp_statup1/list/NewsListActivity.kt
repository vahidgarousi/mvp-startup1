package com.example.mvp_statup1.list

import android.os.*
import android.support.v7.widget.*
import com.example.mvp_statup1.*
import com.example.mvp_statup1.base.*
import com.example.mvp_statup1.data.*
import com.example.mvp_statup1.home.*
import kotlinx.android.synthetic.main.activity_news_list.*

class NewsListActivity : BaseActivity() {
    private var newsList: List<News>? = null
    override fun setProgressIndicator(shouldShow: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)
        if (intent.extras != null) {
            newsList = intent.getParcelableArrayListExtra(EXTRA_KEY_NEWS)
            if (newsList == null) {
                finish()
            }
        } else {
            finish()
        }
        setupViews()
        setupToolbar()
    }

    private fun setupToolbar() {
        iv_newsListActivity_backBtn.setOnClickListener {
            finish()
        }
    }

    private fun setupViews() {
        rv_newsList_newsList.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_newsList_newsList.adapter = NewsAdapter(newsList)
    }

    companion object {
        const val EXTRA_KEY_NEWS = "news"
    }
}
