package com.example.mvp_statup1

import android.os.*
import android.support.design.widget.*
import android.view.*
import androidx.navigation.Navigation.*
import com.example.mvp_statup1.base.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private val bnvItemClickListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                when (menuItem.itemId) {
                    R.id.menu_main_home -> {
                        findNavController(
                            this@MainActivity,
                            R.id.nav_host
                        ).navigate(R.id.homeFragment)
                        return true
                    }
                    R.id.menu_main_category -> {
                        findNavController(
                            this@MainActivity,
                            R.id.nav_host
                        ).navigate(R.id.categoryFragment)
                        return true
                    }
                }
                return true
            }

        }

    override fun setProgressIndicator(shouldShow: Boolean) {
        if (shouldShow) {
            fl_main_progressContainer.visibility = View.VISIBLE
        } else {
            fl_main_progressContainer.visibility = View.GONE

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bnv_main_menu.setOnNavigationItemSelectedListener(bnvItemClickListener)
    }

}
