package com.example.mvp_statup1.base

interface BasePresenter<T : BaseView> {
    fun attachView(view: T)
    fun detachView()
}