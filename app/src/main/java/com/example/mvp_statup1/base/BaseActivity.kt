package com.example.mvp_statup1.base

import android.support.v7.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    abstract fun setProgressIndicator(shouldShow: Boolean)
}