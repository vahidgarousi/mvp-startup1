package com.example.mvp_statup1.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.mvp_statup1.MainActivity

abstract class BaseFragment : Fragment() {
    protected var rootView: View? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(getLayoutRes(), container, false)
            setupViews()
        }
        return rootView
    }

    abstract fun getLayoutRes(): Int
    abstract fun setupViews()

    fun getBaseActivity(): MainActivity {
        return activity as MainActivity
    }
}