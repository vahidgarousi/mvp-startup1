package com.example.mvp_statup1.base

import android.content.Context

interface BaseView {
    fun setProgressIndicator(shouldShow: Boolean)
    fun context(): Context
    fun showError(errorMessage: String)
}