package com.example.mvp_statup1.detailes

import android.os.*
import android.support.v4.content.*
import android.support.v7.app.*
import android.view.*
import cn.jzvd.*
import com.example.mvp_statup1.R
import com.example.mvp_statup1.data.*
import com.squareup.picasso.*
import kotlinx.android.synthetic.main.activity_news_detail.*

class NewsDetailActivity : AppCompatActivity() {
    var news: News? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)
        news = intent.getParcelableExtra(EXTRA_KEY_NEWS_DETAIL)
        setupViews()
    }

    private fun setupViews() {
        if (news != null) {
            setSupportActionBar(tb_newsDetail_toolbar)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeButtonEnabled(true)
            supportActionBar!!.setTitle(news!!.title)
        }
        ctl_newsDetail_main.setExpandedTitleColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )
        ctl_newsDetail_main.setCollapsedTitleTextColor(
            ContextCompat.getColor(
                this,
                android.R.color.white
            )
        )
        ctl_newsDetail_main.title = news!!.title
        if (news!!.isVideoNews) {
            fl_newsDetail_videoContainer.visibility = View.VISIBLE
        } else {
            fl_newsDetail_videoContainer.visibility = View.GONE
        }
        videoPlayer_newsDetails.setUp(news!!.video, JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL)
        Picasso.get().load(news!!.image).into(iv_newsDeatil_newsImage)
        videoPlayer_newsDetails.fullscreenButton.visibility = View.GONE

    }

    companion object {
        const val EXTRA_KEY_NEWS_DETAIL = "news_detail"
    }
}
