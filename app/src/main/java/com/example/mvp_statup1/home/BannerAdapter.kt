package com.example.mvp_statup1.home

import android.support.v7.widget.*
import android.view.*
import android.widget.*
import com.example.mvp_statup1.data.*
import com.squareup.picasso.*

import com.example.mvp_statup1.R

class BannerAdapter : RecyclerView.Adapter<BannerAdapter.BannerViewHolder>() {
    private var banners: List<Banner> = arrayListOf()

    override fun onCreateViewHolder(viewgroup: ViewGroup, position: Int): BannerViewHolder {
        return BannerViewHolder(
            LayoutInflater.from(viewgroup.context).inflate(
                R.layout.item_banner,
                viewgroup,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return banners.size
    }

    override fun onBindViewHolder(holder: BannerViewHolder, position: Int) {
        holder.bindBanner(banners.get(position))
    }

    fun setList(bannerList: List<Banner>) {
        this.banners = bannerList
        notifyDataSetChanged()
    }
    class BannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageBannerIv = itemView.findViewById<ImageView>(R.id.iv_itemBanner_image)
        var titleTv = itemView.findViewById<TextView>(R.id.tv_itemBanner_title)
        var sourceTv = itemView.findViewById<TextView>(R.id.tv_itemBanner_source)
        fun bindBanner(banner: Banner) {
            Picasso.get().load(banner.image).into(imageBannerIv)
            titleTv.text = banner.title
            sourceTv.text = banner.source
        }

    }
}