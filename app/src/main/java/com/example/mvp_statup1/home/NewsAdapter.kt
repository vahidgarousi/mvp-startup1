package com.example.mvp_statup1.home

import android.content.*
import android.net.*
import android.support.v7.widget.*
import android.view.*
import android.widget.*
import com.example.mvp_statup1.R
import com.example.mvp_statup1.data.*
import com.example.mvp_statup1.detailes.*
import com.example.mvp_statup1.utli.*
import com.squareup.picasso.*

class NewsAdapter(newsList: List<News>?) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private var newList: List<News> = arrayListOf()

    init {
        if (newsList != null) {
            this.newList = newsList
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): NewsViewHolder {
        return NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_news,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return newList.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bindNews(newList.get(position))
    }

    fun setList(newsList: List<News>) {
        this.newList = newsList
        notifyDataSetChanged()
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleTV = itemView.findViewById<TextView>(R.id.tv_itemNews_title)
        var contentTV = itemView.findViewById<TextView>(R.id.tv_itemNews_content)
        var dateTV = itemView.findViewById<TextView>(R.id.tv_itemNews_date)
        var imageIV = itemView.findViewById<ImageView>(R.id.iv_itemNews_image)
        var imageVideoIndicator = itemView.findViewById<ImageView>(R.id.iv_itemNews_videoIndicator)
        fun bindNews(news: News) {
            titleTV.text = news.title
            contentTV.text = news.content
            dateTV.text = VCalendarHelper.g2j(news.date, "dd/MM/yyyy")
            Picasso.get().load(Uri.parse(news.image)).into(imageIV)
            if (news.isVideoNews) {
                imageVideoIndicator.visibility = View.VISIBLE
            } else {
                imageVideoIndicator.visibility = View.GONE
            }
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.EXTRA_KEY_NEWS_DETAIL, news)
                itemView.context.startActivity(intent)
            }
        }

    }

}
