package com.example.mvp_statup1.home

import android.content.*
import android.os.*
import android.support.v7.widget.*
import android.widget.*
import com.example.mvp_statup1.*
import com.example.mvp_statup1.base.*
import com.example.mvp_statup1.data.*
import com.example.mvp_statup1.list.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class HomeFragment : BaseFragment(), HomeContract.View {
    private var presenter: HomeContract.Presenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = HomePresenter(NewsRepository(context!!))
    }

    override fun showNews(newsList: List<News>) {
        rv_homeFragment_newsList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val newsAdapter = NewsAdapter(newsList)
        rv_homeFragment_newsList.adapter = newsAdapter
        newsAdapter.setList(newsList)
        iv_homeFragment_latestNews_viewAll.setOnClickListener {
            val intent = Intent(activity, NewsListActivity::class.java)
            intent.putParcelableArrayListExtra(
                NewsListActivity.EXTRA_KEY_NEWS,
                newsList as ArrayList<out Parcelable>
            )
            startActivity(intent)
        }
    }

    override fun showBanners(banners: List<Banner>) {
        rv_homeFragment_banners.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val bannerAdapter = BannerAdapter()
        rv_homeFragment_banners.adapter = bannerAdapter
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(rv_homeFragment_banners)
        bannerAdapter.setList(banners)
    }

    override fun setProgressIndicator(shouldShow: Boolean) {
        getBaseActivity().setProgressIndicator(shouldShow)
    }

    override fun context(): Context {
        return context!!
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(
            context!!,
            rootView!!.rootView.context.getString(R.string.all_unknown_error),
            Toast.LENGTH_LONG
        )
            .show()
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_home
    }

    override fun setupViews() {

    }

    override fun onStart() {
        super.onStart()
        presenter!!.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        presenter!!.detachView()
    }
}