package com.example.mvp_statup1.home

import com.example.mvp_statup1.*
import com.example.mvp_statup1.data.*
import io.reactivex.*
import io.reactivex.android.schedulers.*
import io.reactivex.disposables.*
import io.reactivex.schedulers.*
import org.reactivestreams.*

class HomePresenter(newsDataSource: NewsDataSource) : HomeContract.Presenter {

    private var view: HomeContract.View? = null
    private var newsDataSource: NewsDataSource? = null
    private var compositeDisposable = CompositeDisposable()
    private var isViewRendered: Boolean = false
    private var subscription: Subscription? = null

    init {
        this.newsDataSource = newsDataSource
    }

    override fun attachView(view: HomeContract.View) {
        if (!isViewRendered) {
            this.view = view
            getBannersList()
            getNewsList()
        }
    }

    override fun detachView() {
        this.view = null
        if (compositeDisposable != null && compositeDisposable.size() > 0) {
            compositeDisposable.clear()
        }
        if (subscription != null) {
            subscription!!.cancel()
        }
    }


    override fun getNewsList() {
        view!!.setProgressIndicator(true)
        newsDataSource!!.getNews()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { news ->
                news?.let {
                    view!!.showNews(it)
                    view!!.setProgressIndicator(false)
                    isViewRendered = true
                }
            }
            .doOnError {
                view!!.showError(view!!.context().getString(R.string.all_unknownError))
                view!!.setProgressIndicator(false)
            }
            .doOnSubscribe {
                this.subscription = it
            }
            .subscribe()
    }

    override fun getBannersList() {
        view!!.setProgressIndicator(true)
        newsDataSource!!.getBanners().subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<List<Banner>> {
                override fun onSuccess(banners: List<Banner>) {
                    view!!.showBanners(banners)
                    view!!.setProgressIndicator(false)
                    isViewRendered = true
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onError(e: Throwable) {
                    view!!.showError(view!!.context().getString(R.string.all_unknown_error))
                    view!!.setProgressIndicator(false)
                }

            })
    }


}