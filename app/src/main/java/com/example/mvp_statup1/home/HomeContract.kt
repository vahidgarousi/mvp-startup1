package com.example.mvp_statup1.home

import com.example.mvp_statup1.base.BasePresenter
import com.example.mvp_statup1.base.BaseView
import com.example.mvp_statup1.data.Banner
import com.example.mvp_statup1.data.News

interface HomeContract {
    interface View : BaseView {
        fun showNews(newsList: List<News>)
        fun showBanners(banners: List<Banner>)
    }

    interface Presenter : BasePresenter<View> {
        fun getNewsList()
        fun getBannersList()
    }
}