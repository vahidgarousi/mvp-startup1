package com.example.mvp_statup1.category

import android.content.*
import android.support.design.widget.*
import android.support.v7.widget.*
import android.view.*
import android.widget.*
import com.example.mvp_statup1.*

class CategoryAdapter(var context: Context) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var categories = arrayOfNulls<Category>(6)

    init {
        val topStories = Category()
        topStories.id = 0
        topStories.title = context.getString(R.string.category_topStories)
        topStories.icon = R.drawable.newspaper
        categories[0] = topStories

        val world = Category()
        world.id = 1
        world.title = context.getString(R.string.category_World)
        world.icon = R.drawable.earth
        categories[1] = world

        val business = Category()
        business.id = 2
        business.title = context.getString(R.string.category_business)
        business.icon = R.drawable.domain
        categories[2] = business

        val iran = Category()
        iran.id = 3
        iran.title = context.getString(R.string.category_iran)
        iran.icon = R.drawable.flag
        categories[3] = iran

        val health = Category()
        health.id = 4
        health.title = context.getString(R.string.category_health)
        health.icon = R.drawable.heart_pulse
        categories[4] = health

        val technology = Category()
        technology.id = 5
        technology.title = context.getString(R.string.category_technology)
        technology.icon = R.drawable.chip
        categories[5] = technology

    }

    override fun onCreateViewHolder(viewgroup: ViewGroup, position: Int): CategoryViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(viewgroup.context).inflate(
                R.layout.item_category,
                viewgroup,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        categories[position]?.let { holder.bindCategory(it) }
    }

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var titleCategoryTV = itemView.findViewById<TextView>(R.id.tv_itemCategory_title)
        var iconCategoryIV = itemView.findViewById<ImageView>(R.id.iv_itemCategory_icon)
        fun bindCategory(category: Category) {
            titleCategoryTV.text = category.title
            iconCategoryIV.setImageResource(category.icon)
            itemView.setOnClickListener {
                Snackbar.make(
                    itemView,
                    "Item with title ${category.title} Clicked",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
        }

    }
}