package com.example.mvp_statup1.category

import android.support.v7.widget.*
import com.example.mvp_statup1.*
import com.example.mvp_statup1.base.*

class CategoryFragment : BaseFragment() {

    override fun getLayoutRes(): Int {
        return R.layout.fragment_category
    }

    override fun setupViews() {
        val categoryRV =
            rootView!!.findViewById<RecyclerView>(R.id.rv_categoryFragment_categoryList)
        categoryRV.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val categoryAdapter = context?.let { CategoryAdapter(it) }
        categoryRV.adapter = categoryAdapter
    }
}